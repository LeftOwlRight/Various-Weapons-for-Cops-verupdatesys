dofile(ModPath .. 'automenubuilder.lua')

VariousWeaponForCops = VariousWeaponForCops or {
    settings = {
        Dangerous_mode = false,
        Risk_factor = 5,
        No_Zeal_Shotgunner = false,
        -- No_Flame = false,
        Reduce_Iframe = false,
        Data_differs = false,
        Data_Fix = true,
        -- Dont_Be_Annoying = true,
        Keep_the_Weapon = true,
        Openmod = true,
        A_F = false,
        update_block = false,
        Test_Mode = {
            Test_Enable = false,
            Record_Weapons = false,
            Record_Units = false,
            All_mini = false --[[, All_flame = false, All_dmr = false--]]
        },
        Throwing_Grenades = {
            Th_Enable = false,
            Th_Bots = {
                ThG_Bots_Bow = {
                    ThG_Bots_Bow_Enable = false,
                    ThG_Bots_Min_Bow = 50,
                    ThG_Bots_Max_Bow = 200
                },
                ThG_Bots_Grenade = {
                    ThG_Bots_Grenade_Enable = false,
                    ThG_Bots_Min_Grenade = 100,
                    ThG_Bots_Max_Grenade = 250
                }
            },
            Th_Cops = {
                ThG_Cops_Enable = false,
                ThG_Cops_Min = 50,
                ThG_Cops_Max = 250
            }
        },
        Enemy_bullet_impact = {
            EBI_Enable = false,
            EBI_Value = 30
        },
        Custom_Random = {
            CR_Enable = false,
            default_weapon = 315,
            mini = 40,
            c45 = 10,
            snowthrower = 5,
            m249 = 75,
            m4 = 50,
            g36 = 125,
            r870 = 150,
            raging_bull = 50,
            mp5 = 50,
            ak47 = 50,
            saiga = 50,
            ump = 25,
            beretta92 = 20,
            mossberg = 5,
            benelli = 50,
            --[[heavy_zeal_sniper = 40,
                m14_sniper_npc = 0,
                svdsil_snp = 0,--]]
            rpk_lmg = 25,
            --[[m4_yellow = 30,
                smoke = 5,--]]
            mac11 = 15,
            sg417 = 10
            -- flamethrower = 20,
            -- dmr = 5
        }
    },
    values = {
        ThG_Bots_Min_Bow = {1, 600, 1},
        ThG_Bots_Max_Bow = {1, 600, 1},
        ThG_Bots_Min_Grenade = {1, 600, 1},
        ThG_Bots_Max_Grenade = {1, 600, 1},
        ThG_Cops_Min = {1, 600, 1},
        ThG_Cops_Max = {1, 600, 1},
        Risk_factor = {10, 250, 1},
        EBI_Value = {1, 100, 1},
        default_weapon = {2, 1000, 1},
        mini = {0, 1000, 1},
        c45 = {0, 1000, 1},
        snowthrower = {0, 1000, 1},
        m249 = {0, 1000, 1},
        m4 = {0, 1000, 1},
        g36 = {0, 1000, 1},
        r870 = {0, 1000, 1},
        raging_bull = {0, 1000, 1},
        mp5 = {0, 1000, 1},
        ak47 = {0, 1000, 1},
        saiga = {0, 1000, 1},
        ump = {0, 1000, 1},
        beretta92 = {0, 1000, 1},
        mossberg = {0, 1000, 1},
        benelli = {0, 1000, 1},
        --[[heavy_zeal_sniper = {0, 1000, 1},
            m14_sniper_npc = {0, 1000, 1},
            svdsil_snp = {0, 1000, 1},--]]
        rpk_lmg = {0, 1000, 1},
        --[[m4_yellow = {0, 1000, 1},
            smoke = {0, 1000, 1},--]]
        mac11 = {0, 1000, 1},
        sg417 = {0, 1000, 1}
        -- flamethrower = {0, 1000, 1},
        -- dmr = {0, 1000, 1}
    },
    order = {
        Th_Enable = 200,
        Th_Bots = 100,
        Th_Cops = 10,
    
        ThG_Bots_Bow = 100,
        ThG_Bots_Grenade = 10,
    
        ThG_Bots_Bow_Enable = 100,
        ThG_Bots_Min_Bow = 75,
        ThG_Bots_Max_Bow = 50,
    
        ThG_Bots_Grenade_Enable = 100,
        ThG_Bots_Min_Grenade = 75,
        ThG_Bots_Max_Grenade = 50,
    
        ThG_Cops_Enable = 100,
        ThG_Cops_Min = 75,
        ThG_Cops_Max = 50,

        Openmod = 100,
        Keep_the_Weapon = 97,
        Dangerous_mode = 95,
        Risk_factor = 90,
        Reduce_Iframe = 85,
        No_Zeal_Shotgunner = 80,
        -- No_Flame = 77,
        -- Dont_Be_Annoying = 75,
        Data_Fix = 72,
        Data_differs = 70,
        A_F = 65,
        Enemy_bullet_impact = -99,
        Record_Weapons = -100,
        Record_Units = -150,
        Throwing_Grenades = -200,
        Custom_Random = -500,
        CR_Enable = -500,
        default_weapon = -501,
        m4 = -502,
        ak47 = -503,
        g36 = -504,
        mp5 = -505,
        r870 = -506,
        c45 = -507,
        mini = -508,
        raging_bull = -600,
        saiga = -600,
        ump = -600,
        beretta92 = -600,
        mossberg = -600,
        benelli = -600,
        rpk_lmg = -600,
        --[[m4_yellow = -649,
            smoke = -650,--]]
        mac11 = -600,
        sg417 = -600,
        snowthrower = -601,
        --[[flamethrower = -650,
            dmr = -650,
            heavy_zeal_sniper = -798,
            m14_sniper_npc = -799,
            svdsil_snp = -800,--]]
        Test_Mode = -999
    }
}

Hooks:Add('MenuManagerBuildCustomMenus', 'MenuManagerBuildCustomMenusRandomWeapon', function(menu_manager, nodes)
    AutoMenuBuilder:load_settings(VariousWeaponForCops.settings, 'various_weapon_for_cops')
    AutoMenuBuilder:create_menu_from_table(nodes, VariousWeaponForCops.settings, 'various_weapon_for_cops',
        'blt_options', VariousWeaponForCops.values, VariousWeaponForCops.order)
end)

-- localization
dofile(ModPath .. "menu/localization.lua")
