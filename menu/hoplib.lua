VariousWeaponForCops = {}

VariousWeaponForCops.settings = {
    Dangerous_mode = false,
    Risk_factor = 5,
    No_Zeal_Shotgunner = false,
    -- No_Flame = false,
    Reduce_Iframe = false,
    Data_differs = false,
    Data_Fix = true,
    -- Dont_Be_Annoying = true,
    Keep_the_Weapon = true,
    Openmod = true,
    A_F = false,
    update_block = false,
    Test_Mode = {
        Test_Enable = false,
        Record_Weapons = false,
        Record_Units = false,
        All_mini = false --[[, All_flame = false, All_dmr = false--]]
    },
    Throwing_Grenades = {
        Th_Enable = false,
        Th_Bots = {
            ThG_Bots_Bow = {
                ThG_Bots_Bow_Enable = false,
                ThG_Bots_Min_Bow = 50,
                ThG_Bots_Max_Bow = 200
            },
            ThG_Bots_Grenade = {
                ThG_Bots_Grenade_Enable = false,
                ThG_Bots_Min_Grenade = 100,
                ThG_Bots_Max_Grenade = 250
            }
        },
        Th_Cops = {
            ThG_Cops_Enable = false,
            ThG_Cops_Min = 50,
            ThG_Cops_Max = 250
        }
    },
    Enemy_bullet_impact = {
        EBI_Enable = false,
        EBI_Value = 30
    },
    -- Load_packages = {Load_Enable = false, vit = false, ranc = false},
    Custom_Random = {
        CR_Enable = false,
        default_weapon = 315,
        mini = 40,
        c45 = 10,
        snowthrower = 5,
        m249 = 75,
        m4 = 50,
        g36 = 125,
        r870 = 150,
        raging_bull = 50,
        mp5 = 50,
        ak47 = 50,
        saiga = 50,
        ump = 25,
        beretta92 = 20,
        mossberg = 5,
        benelli = 50,
        --[[heavy_zeal_sniper = 40,
                m14_sniper_npc = 0,
                svdsil_snp = 0,--]]
        rpk_lmg = 25,
        -- m4_yellow = 30,
        -- smoke = 5,
        mac11 = 15,
        sg417 = 10
        -- flamethrower = 20,
        -- dmr = 5
    }
}

VariousWeaponForCops.values = {
    Th_Enable = {
        priority = 200
    },
    Th_Bots = {
        priority = 100
    },
    Th_Cops = {
        priority = 10
    },

    ThG_Bots_Bow = {
        priority = 100
    },
    ThG_Bots_Grenade = {
        priority = 10
    },

    ThG_Bots_Bow_Enable = {
        priority = 100
    },
    ThG_Bots_Min_Bow = {
        min = 1,
        max = 600,
        step = 1,
        priority = 75
    },
    ThG_Bots_Max_Bow = {
        min = 1,
        max = 600,
        step = 1,
        priority = 50
    },

    ThG_Bots_Grenade_Enable = {
        priority = 100
    },
    ThG_Bots_Min_Grenade = {
        min = 1,
        max = 600,
        step = 1,
        priority = 75
    },
    ThG_Bots_Max_Grenade = {
        min = 1,
        max = 600,
        step = 1,
        priority = 50
    },

    ThG_Cops_Enable = {
        priority = 100
    },
    ThG_Cops_Min = {
        min = 1,
        max = 600,
        step = 1,
        priority = 75
    },
    ThG_Cops_Max = {
        min = 1,
        max = 600,
        step = 1,
        priority = 50
    },


    Openmod = {
        priority = 100
    },
    Keep_the_Weapon = {
        priority = 97
    },
    Dangerous_mode = {
        priority = 95
    },
    Risk_factor = {
        min = 10,
        max = 250,
        step = 1,
        priority = 90
    },
    Reduce_Iframe = {
        priority = 85
    },
    No_Zeal_Shotgunner = {
        priority = 80
    },
    --[[No_Flame = {
                priority = 77
            },
            Dont_Be_Annoying = {
                priority = 75
            },--]]
    Data_Fix = {
        priority = 72
    },
    Data_differs = {
        priority = 70
    },
    A_F = {
        priority = 65
    },
    EBI_Value = {
        min = 1,
        max = 100,
        step = 1,
        priority = -98
    },
    Enemy_bullet_impact = {
        priority = -99
    },
    Record_Weapons = {
        priority = -100
    },
    Record_Units = {
        priority = -150
    },
    Throwing_Grenades = {
        priority = -200
    },
    Test_Mode = {
        priority = -999
    },
    CR_Enable = {
        priority = -500
    },
    default_weapon = {
        min = 2,
        max = 1000,
        step = 1,
        priority = -501
    },
    mini = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -508
    },
    c45 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -507
    },
    snowthrower = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -601
    },
    m249 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    m4 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -502
    },
    g36 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -504
    },
    r870 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -506
    },
    raging_bull = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    mp5 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -505
    },
    ak47 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -503
    },
    saiga = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    ump = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    beretta92 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    mossberg = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    benelli = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    --[[heavy_zeal_sniper = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -798
            },
            m14_sniper_npc = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -799
            },
            svdsil_snp = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -800
            },--]]
    rpk_lmg = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    --[[m4_yellow = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -649
            },
            smoke = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -650
            },--]]
    mac11 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    },
    sg417 = {
        min = 0,
        max = 1000,
        step = 1,
        priority = -600
    }
    --[[flamethrower = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -650
            },
            dmr = {
                min = 0,
                max = 1000,
                step = 1,
                priority = -650
            }--]]
}

local builder = MenuBuilder:new("various_weapon_for_cops", VariousWeaponForCops.settings, VariousWeaponForCops.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusRandomWeapon", function(menu_manager, nodes)
    builder:create_menu(nodes)
end)

-- localization
dofile(ModPath .. "menu/localization.lua")


VWC_LtyR_Settings = VariousWeaponForCops.settings