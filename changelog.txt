v8.731
Fix a crash -- btg.lua:75: attempt to index a nil value

v8.72
Slightly optimize performance
Add more checks to prevent accident
Ask Bots to throw grenades only to their target

v8.71
Fix that NPC throwing grenades function might be broken
Prevent Marshal Shield from getting a random weapon

v8.7
Optimized the code of NPC Throwing Grenades
Potencially fix that Jokers also throw grenades to hurt players
Fix that some friendly NPC throw grenades to hurt players
Now only law enforces will throw grenades
Enemies now also throw grenades to clients
Enemies now only throw when they see players
Add throwing animation for enemies
Allow you to customize the delay of throwing grenades/arrows or disable them

v8.61
Update to 232.4 (remove event stuff)

v8.6
Add a new weapon - SnowThrower, only appears when playing event mode.
Make bots shout when they throw grenades.

v8.52
Add 2 new weapons

v8.41
Remove all broken weapon I know to fix the crash
Add a setting to record what unit uses what weapon

v8.3
Simply remove 'flamethrower' from weapon list since U226 broke it (maybe earlier)
Give me a feedback if any other weapon is broken since that

v8.2
Fix an issue of randomness I forgot since v6.94

v8.1
Fix minifix(Data Fix) broken

v8.0
Use Hoplib to build the menu
Allow you to customize the chance of weapon got by enemies

v7.14
Someone asked for a No-Flamethrower mode. (haven't verified though)

v7.13
Add 3 options in Developer mode. You guys can also use them as mutator. (All minigun, dmr, flamethrower) You'll need to enable Test Mode first.
CheatWorkShop moment : Enable "All minigun" and disable both "Enable Random Weapon" and "Keep the Weapon" before creating a heist, enter the heist, enable "Enable Random Weapon" before converting an enemy. Then, you get a minigun joker!
Be Aware! This is just a Test Mode, if you let some rare units like heavy_zeal_sniper(crime spree) get a minigun or sth, you may crash. I tried to add a check to prevent this issue, but somehow it won't work after adding the check. I'd have to find out what is wrong someday.

v7.1
Add marshal_marksman's weapon into random list and prevent him from getting a random weapon.

v7.01
Fix me forgetting adding English localization for Data Fix

v7.0
Allow you to disable minifix through settings (Data Fix)
minifix: Fix enemies using minigun with single shot.

v6.94
--Fix an issue about randomness since v2.0
Special blame to Vi.night. lol

v6.931
--Remove a useless stuff that may cause unstable problem
--Fix a critical mistake : NPC Throwing Grenades was always running and you couldn't disable it with settings

v6.92
--Fix a crash

v6.91
--Update Dangerous mode.
· Add flamethrower to encounter dodge perk deck (You can't dodge the damage of flamethrower)
· Spare mercy to Grinder and Maniac on DS (They didn't have a chance to survive on DS with this mod)

v6.9
--Allow you to enable the function of 2022 April Fool : Cops Spinning

v6.81
Happy April Fool!

v6.8
--Fix a crash

v6.7
--Fix a crash

v6.6
--Try to fix the crash.

v6.5
--Give you a way to disable the function that causes crash.

Application has crashed: C++ exception
mods/Various-Weapons-for-Cops-verupdatesys-main/btg.lua:172: attempt to perform arithmetic on a nil value
Someone has reported this crash (happens when enable Cops Throwing Grenades). I will try to fix it when I have free time.
If you meet it as well, you can just disable the Cops Throwing Grenades.
It should be fixed in v6.8

v6.3
--Try to fix a potencial crash. (FAILED)

v6.2
--Fix few bugs.

v6.0
--Add a new function:
· NPC Throwing Grenades : NPC can now throw/shoot some grenades at set intervals. These grenades can hurt you and themselves no matter who throws them. Tell me if you have crash with it. hpaf

v5.9
--Fix enemies shooting r870 like izhma.

v5.8
--Add minigun support for more units.
--Increase the chance of heavy_swat and swat getting a minigun.
--Add Izhma support for units on DS.

v5.6
--On DS, Heavy Zeal Swat can now correctly use minigun. (may not synced though)
Before this, they use minigun with single shot;
Now, they use minigun with full power.
This function haven't completed yet. I will soon add more support for those unit that can not correctly use some weapons, and may fix their damage.

v5.4
--Fix a potencial crash.

v5.3
--Fix a crash happening when Enemy Bullet Impact is enabled.
mods/Various-Weapons-for-Cops-verupdatesys-main/ebi.lua:10: attempt to call method 'get_real_armor' (a nil value)

v5.2
--Add flamethrower for enemies.
Warning: For those who are using wolfhud or the old version of vanilla hud, you get crash.

v5.1
--Put settings in order
--Fix few loc
--New function:
· Enemy Bullet Impact : Your screen will shakes when taking damage with no armor. (Similar to CSGO)
However, this function needs feedback. I'm not sure if it works well, especially what default value "Impact Factor" should be. Some players may also feel dizzy with it.
It may also cause problem between perk decks in balance, tell me your opinion and I may do something for certain perk deck.
Already got reports : Conflict with No Screen Shake!!

v4.0
--Add an option: you can now keep an enemy's weapon when he is converted into joker.

v3.7
-Fix a stupid mistake from v3.5, which may cause crash.

v3.6
--Fix localization that I forgot

v3.5
--Slightly improve Dangours mode and code notes.

v3.4
--Move the auto update system to GitLab, so some players can enjoy it as well.

v3.3
--Add an option that logs weapons.
--Format Codes.

v3.2
--Fix the problem that wasn't fixed well in v3.1.

v3.1
--Fix a problem that there would still be shotgunners when both No Zeal Shotgunners and Dangerous mode are enabled.

v3.0
--Initial "Dangerous mode"
--Add 3 options:
· Risk factor (Add a weapon into weapon list how many times when specific perk deck detected)
· Data differs (The data changes are not sync, so I make it an option)
· Dont be annoying (The sniper is so loud that it is very annoying!!!)

v2.7
--Fix a PlayerDamage crash

v2.6
--Add auto update system. (github)
· You'll need to manually download v2.6, otherwise there won't be auto update.

v2.5
--Add 2 options :
1) No Zeal Shotgunners (as shotgunners are not friendly to Kingpin)
2) Reduced I-Frame Damage (already got permission from Hoppip)
· Haven't tested it yet. You should keep a 2.4 backup and…if you find any issue, plz tell me. (I somehow lost the backup of 2.4)
Sorry, I'm still leaving the Dangerous mode undone lol

v2.4
--Correct a stupid mistake which causes the mod invalid. (means that v2.3 is un-play-able)
· not work before PT -Dec 09 2021 8:50:00 AM because I'm too stupid.

v2.3 (invalid)
--Fix few potential crashes. (some appear in v2.2, one was not done well in v2.1)

v2.2
--Rework some random codes to optimize performance.
· In v2.1, if there're too many weapon_id in weapon list, the game will drop framerate when spawning enemies
--Add 2 options:
1) Dangerous mode——still working in progress, has no effect in this version.
2) Enable or disable the mod——takes effect immediately, no need to restart the heist.
··· Special thanks to Hoppip's Auto Menu Builder and his help.

v2.1
--Completely disable the function when using as a client to prevent a crash. (this was not done well)

v2.0
--Code optimized. (Rework)
--Increase randomness.
--Cloakers no longer have random weapons. (Avoid potential crash)
--Add more weapons.
Special thanks to Vi.night.

v1.2
--Add more weapons to fbi_swat, fbi_heavy_swat and city_swat(risk level 2~5) as they have the correct preset.

v1.1
--Fix a crash when heavy zeal sniper has a wrong weapon. (Tell me if there's any other unit has the same issue)
[string "lib/units/enemies/cop/logics/coplogictravel.lua"]:135: attempt to index a nil value
[string "lib/units/enemies/cop/actions/full_body/copac..."]:1630: attempt to index local 'weapon_usage_tweak' (a nil value)

v1.0
--Initial release