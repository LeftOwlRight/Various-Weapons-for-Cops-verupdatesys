if HopLib then
	dofile(ModPath .. "menu/hoplib.lua")
else
	dofile(ModPath .. "menu/amb.lua")
end

--[[

{
			"hook_id" : "lib/setups/gamesetup", "script_path" : "Core.lua"
		},


if not VariousWeaponForCops.settings.Load_packages.Load_Enable then
	return
end

local old_load_mad = GameSetup.load_packages
	function GameSetup:load_packages()
		old_load_mad(self)
		local pac_kage_mad = "packages/lvl_mad"
		if not PackageManager:loaded(pac_kage_mad) then
			PackageManager:load(pac_kage_mad)
		end
	end

if VariousWeaponForCops.settings.Load_packages.ranc then
	local old_load = GameSetup.load_packages
	function GameSetup:load_packages()
		old_load(self)
		local pac_kage = "packages/job_ranc"
		if not PackageManager:loaded(pac_kage) then
			PackageManager:load(pac_kage)
		end
	end
	log("packages ranc loaded")
end

if VariousWeaponForCops.settings.Load_packages.vit then
	local old_load = GameSetup.load_packages
	function GameSetup:load_packages()
		old_load(self)
		local pac_kage = "packages/dlcs/vit/job_vit"
		if not PackageManager:loaded(pac_kage) then
			PackageManager:load(pac_kage)
		end
	end
	log("packages vit loaded")
end--]]