--April Fool
local getaf = os.date('*t')

if RequiredScript == 'lib/units/enemies/cop/copmovement' then
    Hooks:PostHook(
        CopMovement,
        'update',
        'update_betterer_cops',
        function(self, unit, t, dt)
            if VariousWeaponForCops.settings.A_F then --for _, v in pairs(managers.groupai:state():is_unit_team_AI(self._unit)) do --managers.groupai:state():is_unit_team_AI(self._unit)
                if getmetatable(self) == CopMovement then --if managers.groupai:state():is_unit_team_AI(self._unit) then--if v.unit ~= self._unit then
                    --if getaf.month == 4 and getaf.day == 1 then
                    self._unit:set_local_rotation(Rotation:yaw_pitch_roll((t * 1000) % 360, 0, 0)) --break
                    --end
                end
            end
        end
    )
end

--client
if Network:is_client() then
    return
end

Hooks:PostHook(
    GroupAIStateBase,
    'on_enemy_weapons_hot',
    'game_went_loud_stuff_asdf',
    function()
        function CopBase:default_weapon_name()
            --So Enemies can keep their weapons
            if VariousWeaponForCops.settings.Keep_the_Weapon then
                if self._selected_weapon then
                    return self._selected_weapon
                end
            end

            -- if not VariousWeaponForCops.settings.Openmod then
            -- 	managers.chat:send_message(1, peer, 'mod disabled')
            -- end

            --not sync stuffs
            if VariousWeaponForCops.settings.Data_differs then
                tweak_data.weapon.raging_bull_npc.DAMAGE = 6
                tweak_data.weapon.ak47_ass_npc.auto.fire_rate = 0.18
            --tweak_data.character
            end

            --minifix
            if VariousWeaponForCops.settings.Data_Fix then
                --izhma presets
                tweak_data.character.presets.weapon.deathwish.is_shotgun_mag =
                    deep_clone(tweak_data.character.presets.weapon.deathwish.is_shotgun_pump)
                tweak_data.character.presets.weapon.deathwish.is_shotgun_mag.FALLOFF = {
                    {dmg_mul = 3.60, r = 100, acc = {0.7, 0.9}, recoil = {0.10, 0.20}, mode = {1, 0, 0, 0}},
                    {dmg_mul = 3.40, r = 500, acc = {0.5, 0.75}, recoil = {0.15, 0.25}, mode = {1, 0, 0, 0}},
                    {dmg_mul = 3.20, r = 1000, acc = {0.3, 0.6}, recoil = {0.20, 0.30}, mode = {1, 0, 0, 0}},
                    {dmg_mul = 2.00, r = 2000, acc = {0.25, 0.55}, recoil = {0.25, 0.35}, mode = {1, 0, 0, 0}},
                    {dmg_mul = 0.80, r = 3000, acc = {0.15, 0.5}, recoil = {0.25, 0.35}, mode = {1, 0, 0, 0}}
                }

                --Heavy Swat
                --mini
                tweak_data.character.heavy_swat.weapon.mini.aim_delay = {
                    0,
                    0
                }
                tweak_data.character.heavy_swat.weapon.mini.focus_delay = 0
                tweak_data.character.heavy_swat.weapon.mini.FALLOFF = {
                    {dmg_mul = 1.00, r = 100, acc = {0.7, 0.9}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.95, r = 500, acc = {0.5, 0.75}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.90, r = 1000, acc = {0.3, 0.6}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.80, r = 2000, acc = {0.25, 0.55}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.70, r = 3000, acc = {0.15, 0.5}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}}
                }

                --Swat
                --mini
                tweak_data.character.swat.weapon.mini.aim_delay = {
                    0,
                    0
                }
                tweak_data.character.swat.weapon.mini.focus_delay = 0
                tweak_data.character.swat.weapon.mini.FALLOFF = {
                    {dmg_mul = 1.00, r = 100, acc = {0.7, 0.9}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.95, r = 500, acc = {0.5, 0.75}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.90, r = 1000, acc = {0.3, 0.6}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.80, r = 2000, acc = {0.25, 0.55}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.70, r = 3000, acc = {0.15, 0.5}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}}
                }

                --Cop
                --mini
                tweak_data.character.cop.weapon.mini.aim_delay = {
                    0,
                    0
                }
                tweak_data.character.cop.weapon.mini.focus_delay = 0
                tweak_data.character.cop.weapon.mini.FALLOFF = {
                    {dmg_mul = 1.00, r = 100, acc = {0.7, 0.9}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.95, r = 500, acc = {0.5, 0.75}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.90, r = 1000, acc = {0.3, 0.6}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.80, r = 2000, acc = {0.25, 0.55}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.70, r = 3000, acc = {0.15, 0.5}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}}
                }

                --FBI
                --mini
                tweak_data.character.fbi.weapon.mini.aim_delay = {
                    0,
                    0
                }
                tweak_data.character.fbi.weapon.mini.focus_delay = 0
                tweak_data.character.fbi.weapon.mini.FALLOFF = {
                    {dmg_mul = 1.00, r = 100, acc = {0.7, 0.9}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.95, r = 500, acc = {0.5, 0.75}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.90, r = 1000, acc = {0.3, 0.6}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.80, r = 2000, acc = {0.25, 0.55}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.70, r = 3000, acc = {0.15, 0.5}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}}
                }

                --security guard
                --mini
                tweak_data.character.security.weapon.mini.aim_delay = {
                    0,
                    0
                }
                tweak_data.character.security.weapon.mini.focus_delay = 0
                tweak_data.character.security.weapon.mini.FALLOFF = {
                    {dmg_mul = 1.00, r = 100, acc = {0.7, 0.9}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.95, r = 500, acc = {0.5, 0.75}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.90, r = 1000, acc = {0.3, 0.6}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.80, r = 2000, acc = {0.25, 0.55}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}},
                    {dmg_mul = 0.70, r = 3000, acc = {0.15, 0.5}, recoil = {0, 0.01}, mode = {0, 0, 0, 1}}
                }
            end

            --Utils:IsCurrentWeapon(weapontype)
            local trydefault_weapon_id = self._default_weapon_id

            local randomWeapon = {}
            local hasing_tag = {'shield', 'medic', 'taser', 'spooc', 'sniper', 'tank'}
            --math.randomseed(tostring(os.time()):reverse():sub(1, 7))

            weapon_list = {}
            total_weight = 0

            local function addweapon(id, weight) -- Create a data object containing weapon id and its weight (chance)
                local data = {
                    id = id,
                    weight = weight
                }
                table.insert(weapon_list, data) -- Insert the weapon data into the weapon list
                total_weight = total_weight + weight -- Add the weapon weight (chance) to the total weight
            end

            local function random_weapon()
                local rnd = math.random(total_weight) -- Roll a random number between 1 and total_weight
                local index = 1 -- Start at the first element in the weapon list

                while index <= #weapon_list do
                    rnd = rnd - weapon_list[index].weight -- Subtract the weight of the current weapon data from the rolled random number

                    if rnd <= 0 then
                        return weapon_list[index].id -- If the rolled number is smaller or equal to zero, choose that weapon
                    end

                    index = index + 1 --Go to next element in the list
                end
            end

            --addweapon('benelli', 9999)

            if VariousWeaponForCops.settings.Custom_Random.CR_Enable then
                if
                    self._unit:base()._tweak_table == 'heavy_swat_sniper' or
                        self._unit:base()._tweak_table == 'triad_boss' or
                        self._unit:base()._tweak_table == 'marshal_marksman' or
                        self._unit:base()._tweak_table == 'marshal_shield'
                 then
                    addweapon(self._default_weapon_id, 1)
                elseif
                    self._unit:base()._tweak_table == 'city_swat' or self._unit:base()._tweak_table == 'fbi_swat' or
                        self._unit:base()._tweak_table == 'fbi_heavy_swat'
                 then
                    addweapon('mini', VariousWeaponForCops.settings.Custom_Random.mini)
                    addweapon('c45', VariousWeaponForCops.settings.Custom_Random.c45)
                    addweapon('m249', VariousWeaponForCops.settings.Custom_Random.m249)
                    addweapon('m4', VariousWeaponForCops.settings.Custom_Random.m4)
                    addweapon('g36', VariousWeaponForCops.settings.Custom_Random.g36)
                    if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                        addweapon('r870', VariousWeaponForCops.settings.Custom_Random.r870)
                    end
                    addweapon('raging_bull', VariousWeaponForCops.settings.Custom_Random.raging_bull)
                    addweapon('mp5', VariousWeaponForCops.settings.Custom_Random.mp5)
                    addweapon('ak47', VariousWeaponForCops.settings.Custom_Random.ak47)
                    addweapon('saiga', VariousWeaponForCops.settings.Custom_Random.saiga)
                    addweapon('ump', VariousWeaponForCops.settings.Custom_Random.ump)
                    addweapon('beretta92', VariousWeaponForCops.settings.Custom_Random.beretta92)
                    addweapon('mossberg', VariousWeaponForCops.settings.Custom_Random.mossberg)
                    addweapon('benelli', VariousWeaponForCops.settings.Custom_Random.benelli)
                    if not VariousWeaponForCops.settings.update_block then
                        --[[if CrimeNetManager:is_event_active() then
                            addweapon('snowthrower', VariousWeaponForCops.settings.Custom_Random.snowthrower)
                        end--]]
                    end
                    --addweapon('heavy_zeal_sniper', VariousWeaponForCops.settings.Custom_Random.heavy_zeal_sniper)
                    --[[if not VariousWeaponForCops.settings.Dont_Be_Annoying then
                        addweapon('m14_sniper_npc', VariousWeaponForCops.settings.Custom_Random.m14_sniper_npc)
                        addweapon('svdsil_snp', VariousWeaponForCops.settings.Custom_Random.svdsil_snp)
                    end--]]
                    if Global.level_data.level_id == "mad" then
                        addweapon('rpk_lmg', VariousWeaponForCops.settings.Custom_Random.rpk_lmg)
                        --addweapon('m4_yellow', VariousWeaponForCops.settings.Custom_Random.m4_yellow)
                        --addweapon('smoke', VariousWeaponForCops.settings.Custom_Random.smoke)
                        addweapon('mac11', VariousWeaponForCops.settings.Custom_Random.mac11)
                        addweapon('sg417', VariousWeaponForCops.settings.Custom_Random.sg417)
                    end
                    --[[if not VariousWeaponForCops.settings.No_Flame then
                        addweapon('flamethrower', VariousWeaponForCops.settings.Custom_Random.flamethrower)
                    end--]]
                    --addweapon('dmr', VariousWeaponForCops.settings.Custom_Random.dmr)
                    addweapon(self._default_weapon_id, VariousWeaponForCops.settings.Custom_Random.default_weapon)
                else
                    --addweapon('flamethrower', 1000)
                    addweapon('mini', VariousWeaponForCops.settings.Custom_Random.mini)
                    addweapon('m249', VariousWeaponForCops.settings.Custom_Random.m249)
                    addweapon('m4', VariousWeaponForCops.settings.Custom_Random.m4)
                    addweapon('g36', VariousWeaponForCops.settings.Custom_Random.g36)
                    if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                        addweapon('r870', VariousWeaponForCops.settings.Custom_Random.r870)
                    end
                    addweapon('raging_bull', VariousWeaponForCops.settings.Custom_Random.raging_bull)
                    addweapon('mp5', VariousWeaponForCops.settings.Custom_Random.mp5)
                    addweapon('ak47', VariousWeaponForCops.settings.Custom_Random.ak47)
                    addweapon('saiga', VariousWeaponForCops.settings.Custom_Random.saiga)
                    addweapon('ump', VariousWeaponForCops.settings.Custom_Random.ump)
                    addweapon('beretta92', VariousWeaponForCops.settings.Custom_Random.beretta92)
                    addweapon('mossberg', VariousWeaponForCops.settings.Custom_Random.mossberg)
                    addweapon('benelli', VariousWeaponForCops.settings.Custom_Random.benelli)
                    if not VariousWeaponForCops.settings.update_block then
                        --if CrimeNetManager:is_event_active ~= nil and CrimeNetManager:is_event_active() then
                        --[[if CrimeNetManager:is_event_active() then
                            addweapon('snowthrower', VariousWeaponForCops.settings.Custom_Random.snowthrower)
                        end--]]
                    end
                    --[[if not VariousWeaponForCops.settings.No_Flame then
                        addweapon('flamethrower', VariousWeaponForCops.settings.Custom_Random.default_weapon)
                    end--]]
                    --addweapon('dmr', 5)
                    --[[if self._unit:base()._tweak_table == 'heavy_swat' then
                        addweapon('smoke', VariousWeaponForCops.settings.Custom_Random.smoke)
                    end--]]
                    addweapon(self._default_weapon_id, VariousWeaponForCops.settings.Custom_Random.default_weapon)
                end
            else
                if
                    self._unit:base()._tweak_table == 'heavy_swat_sniper' or
                        self._unit:base()._tweak_table == 'triad_boss' or
                        self._unit:base()._tweak_table == 'marshal_marksman' or
                        self._unit:base()._tweak_table == 'marshal_shield'
                 then
                    addweapon(self._default_weapon_id, 1)
                elseif
                    self._unit:base()._tweak_table == 'city_swat' or self._unit:base()._tweak_table == 'fbi_swat' or
                        self._unit:base()._tweak_table == 'fbi_heavy_swat'
                 then
                    addweapon('mini', 40)
                    addweapon('c45', 10)
                    addweapon('m249', 75)
                    addweapon('m4', 50)
                    addweapon('g36', 125)
                    if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                        addweapon('r870', 150)
                    end
                    addweapon('raging_bull', 50)
                    addweapon('mp5', 50)
                    addweapon('ak47', 50)
                    addweapon('saiga', 50)
                    addweapon('ump', 25)
                    addweapon('beretta92', 20)
                    addweapon('mossberg', 5)
                    addweapon('benelli', 50)
                    if not VariousWeaponForCops.settings.update_block then
                        --[[if CrimeNetManager:is_event_active() then
                            addweapon('snowthrower', 5)
                        end--]]
                    end
                    --addweapon('heavy_zeal_sniper', 40)
                    --[[if not VariousWeaponForCops.settings.Dont_Be_Annoying then
                        addweapon('m14_sniper_npc', 7)
                        addweapon('svdsil_snp', 3)
                    end--]]
                    if Global.level_data.level_id == "mad" then
                        addweapon('rpk_lmg', 25)
                        --addweapon('m4_yellow', 30)
                        --addweapon('smoke', 5)
                        addweapon('mac11', 15)
                        addweapon('sg417', 10)
                    end
                    --[[if not VariousWeaponForCops.settings.No_Flame then
                        addweapon('flamethrower', 20)
                    end--]]
                    --addweapon('dmr', 5)
                    addweapon(self._default_weapon_id, 180)
                else
                    addweapon('mini', 20)
                    addweapon('m249', 40)
                    addweapon('m4', 50)
                    addweapon('g36', 202)
                    if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                        addweapon('r870', 200)
                    end
                    addweapon('raging_bull', 50)
                    addweapon('mp5', 50)
                    addweapon('ak47', 50)
                    addweapon('saiga', 5)
                    addweapon('ump', 5)
                    addweapon('beretta92', 10)
                    addweapon('mossberg', 5)
                    addweapon('benelli', 50)
                    if not VariousWeaponForCops.settings.update_block then
                        --[[if CrimeNetManager:is_event_active() then
                            addweapon('snowthrower', 5)
                        end--]]
                    end
                    --[[if not VariousWeaponForCops.settings.No_Flame then
                        addweapon('flamethrower', 20)
                    end--]]
                    --addweapon('dmr', 5)
                    --[[if self._unit:base()._tweak_table == 'heavy_swat' then
                        addweapon('smoke', 15)
                    end--]]
                    addweapon(self._default_weapon_id, 315)
                end
            end

            -- danger or not
            if VariousWeaponForCops.settings.Dangerous_mode then
                local RiskFactorX = VariousWeaponForCops.settings.Risk_factor
                local deck_id = managers.skilltree:get_specialization_value('current_specialization')
                local deck_switch = {
                    --I don't rly think that these perk decks need to be targeted by Dangerous mode.
                    --But if you want, just add them yourself.
                    --I may do what I want them to be in the future.

                    [1] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Crew Chief     --领队
                        end
                    end,
                    [2] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Muscle         --肌肉男
                        end
                    end,
                    [3] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Armored        --军械
                        end
                    end,
                    [4] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Rogue          --浪人
                            --[[if not VariousWeaponForCops.settings.No_Flame then
                                addweapon('flamethrower', RiskFactorX / 10)
                            end--]]
                        end
                    end,
                    [5] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Hitman         --杀手
                        end
                    end,
                    [6] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Crook          --骗术师
                        end
                    end,
                    [7] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Burglar        --夜贼
                        end
                    end,
                    [8] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Infiltrator    --间谍
                        end
                    end,
                    [9] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Sociopath      --反社会
                        end
                    end,
                    [10] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Gambler        --赌徒
                        end
                    end,
                    [11] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Grinder        --前卫        --Easier
                            addweapon('m4', RiskFactorX * 5)
                            addweapon('mp5', RiskFactorX * 3)
                            addweapon('c45', RiskFactorX)
                            addweapon('ump', RiskFactorX)
                        end
                    end,
                    [12] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Yakuza         --极道
                        end
                    end,
                    [13] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --EX-President   --前总统
                        end
                    end,
                    [14] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Maniac         --瘾君子        --Easier
                            addweapon('m4', RiskFactorX * 5)
                            addweapon('mp5', RiskFactorX * 3)
                            addweapon('c45', RiskFactorX)
                            addweapon('ump', RiskFactorX)
                        end
                    end,
                    [15] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Anarchist       --安那奇
                            if
                                self._unit:base()._tweak_table == 'city_swat' or
                                    self._unit:base()._tweak_table == 'fbi_swat' or
                                    self._unit:base()._tweak_table == 'fbi_heavy_swat'
                             then
                                addweapon('mini', RiskFactorX)
                                --addweapon('heavy_zeal_sniper', RiskFactorX)
                            end
                            --[[if not VariousWeaponForCops.settings.No_Flame then
                                addweapon('flamethrower', RiskFactorX / 10)
                            end--]]
                        end
                    end,
                    [16] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Biker           --飞车党
                        end
                    end,
                    [17] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Kingpin         --霸王
                            if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                                addweapon('r870', RiskFactorX)
                            end
                        end
                    end,
                    [18] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Sicario         --行者
                        end
                    end,
                    [19] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Stoic           --修士
                            if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                                addweapon('r870', RiskFactorX)
                            end
                            addweapon('g36', RiskFactorX)
                        end
                    end,
                    [20] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                        --Tag Team        --双人组
                        end
                    end,
                    [21] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Hacker          --黑客
                            --[[if not VariousWeaponForCops.settings.No_Flame then
                                addweapon('flamethrower', RiskFactorX / 10)
                            end--]]
                        end
                    end,
                    [22] = function()
                        if
                            not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                                not self._unit:base()._tweak_table == 'triad_boss' and
                                not self._unit:base()._tweak_table == 'marshal_marksman' and
                                not self._unit:base()._tweak_table == 'marshal_shield'
                         then
                            --Leech           --水蛭
                            if not VariousWeaponForCops.settings.No_Zeal_Shotgunner then
                                addweapon('r870', RiskFactorX)
                            end
                            addweapon('g36', RiskFactorX)
                            addweapon('mp5', RiskFactorX)
                            addweapon('c45', RiskFactorX)
                            --[[if not VariousWeaponForCops.settings.No_Flame then
                                addweapon('flamethrower', RiskFactorX / 5)
                            end--]]
                            if
                                self._unit:base()._tweak_table == 'city_swat' or
                                    self._unit:base()._tweak_table == 'fbi_swat' or
                                    self._unit:base()._tweak_table == 'fbi_heavy_swat'
                             then
                                addweapon('mini', RiskFactorX)
                                addweapon('m249', RiskFactorX)
                            end
                        end
                    end
                }
                local check_deck = deck_switch[deck_id]

                --host
                if check_deck then
                    check_deck()
                else
                    log('Perk deck is not found.')
                end

                --client
                for _, peer in pairs(managers.network:session():peers()) do
                    local skill_data = managers.skilltree:unpack_from_string(peer:skills())
                    local perk_deck_index = skill_data.specializations[1]
                    local check_deck_client = deck_switch[perk_deck_index]
                    if check_deck_client then
                        check_deck_client()
                    else
                        log('Perk deck is not found!')
                    end
                end
            end

            --Test Mode
            if VariousWeaponForCops.settings.Test_Mode.Test_Enable then
                if
                    self._unit:base()._tweak_table == 'city_swat' or self._unit:base()._tweak_table == 'fbi_swat' or
                        self._unit:base()._tweak_table == 'fbi_heavy_swat'
                 then
                --addweapon('dmr', 99999)
                end

                if VariousWeaponForCops.settings.Test_Mode.All_mini then
                    --[[if
                        not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                            not self._unit:base()._tweak_table == 'triad_boss' and
                            not self._unit:base()._tweak_table == 'marshal_marksman'
                     then--]]
                    addweapon('mini', 9999)
                --end
                end

                if VariousWeaponForCops.settings.Test_Mode.All_flame then
                    --[[if
                        not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                            not self._unit:base()._tweak_table == 'triad_boss' and
                            not self._unit:base()._tweak_table == 'marshal_marksman'
                     then--]]
                    --addweapon('flamethrower', 9999)
                --end
                end

                --[[if VariousWeaponForCops.settings.Test_Mode.All_dmr then
                    if
                        not self._unit:base()._tweak_table == 'heavy_swat_sniper' and
                            not self._unit:base()._tweak_table == 'triad_boss' and
                            not self._unit:base()._tweak_table == 'marshal_marksman'
                     then
                    addweapon('dmr', 9999)
                --end
                end--]]
            end

            -- enable or not
            if VariousWeaponForCops.settings.Openmod then
                for _, checktag in ipairs(hasing_tag) do
                    if self:has_tag(checktag) then
                        trydefault_weapon_id = self._default_weapon_id
                        break
                    else
                        trydefault_weapon_id = random_weapon()
                    end
                end
            else
                trydefault_weapon_id = self._default_weapon_id
            end

            --managers.chat:_receive_message(1, "E", trydefault_weapon_id , Color('19FF19'))
            local weap_ids = tweak_data.character.weap_ids
            local unit_carry = self._unit:base()._tweak_table

            for i_weap_id, weap_id in ipairs(weap_ids) do
                if trydefault_weapon_id == weap_id then
                    --log(self._unit:base()._tweak_table)
                    if VariousWeaponForCops.settings.Test_Mode.Record_Units then
                        log('The unit is ' .. unit_carry)
                    end
                    if VariousWeaponForCops.settings.Test_Mode.Record_Weapons then
                        log('His weapon is ' .. trydefault_weapon_id)
                    end
                    self._selected_weapon = tweak_data.character.weap_unit_names[i_weap_id]
                    return tweak_data.character.weap_unit_names[i_weap_id]
                end
            end
        end
    end
)
