VWC_LtyR_Settings_NPC_ThG = VWC_LtyR_Settings.Throwing_Grenades

if VariousWeaponForCops and not VWC_LtyR_Settings_NPC_ThG.Th_Enable then
    return
end

if Network:is_client() then
    return
end

-- 为什么不丢个CLK出来呢 *laugh*

Hooks:PostHook(GroupAIStateBase, 'on_enemy_weapons_hot', 'game_went_loud_stuff_asdf_botbtg', function()

    -- math.randomseed(tostring(os.time()):reverse():sub(1, 7))

    local bot = managers.groupai:state():all_AI_criminals() -- 获取所有AI队友
    -- local cop = managers.enemy:all_enemies() -- 获取所有敌人
    -- local unit_info = {} --为Hoplib准备的init

    --- 描述：用于输出不同的时间间隔设置，1为AI射箭，2为AI丢雷，3为警方丢雷
    ---@param num:number
    ---@return :number,number 1号输出最小时间，2号输出最大时间
    function VWC_btg_Default_time(num)
        -- AI队友丢雷
        if num == 2 then
            local VWC_Min_Bots_Grenade_chance = VWC_LtyR_Settings_NPC_ThG.Th_Bots.ThG_Bots_Grenade.ThG_Bots_Min_Grenade
            local VWC_Max_Bots_Grenade_chance = VWC_LtyR_Settings_NPC_ThG.Th_Bots.ThG_Bots_Grenade.ThG_Bots_Max_Grenade
            local VWC_Sotre_Bots_Grenade_chance = 0
            if VWC_Min_Bots_Grenade_chance > VWC_Max_Bots_Grenade_chance then
                VWC_Sotre_Bots_Grenade_chance = VWC_Min_Bots_Grenade_chance
                VWC_Min_Bots_Grenade_chance = VWC_Max_Bots_Grenade_chance
                VWC_Max_Bots_Grenade_chance = VWC_Sotre_Bots_Grenade_chance
            end
            return VWC_Min_Bots_Grenade_chance, VWC_Max_Bots_Grenade_chance

            -- AI队友射箭
        elseif num == 1 then
            local VWC_Min_Bots_Bow_chance = VWC_LtyR_Settings_NPC_ThG.Th_Bots.ThG_Bots_Bow.ThG_Bots_Min_Bow
            local VWC_Max_Bots_Bow_chance = VWC_LtyR_Settings_NPC_ThG.Th_Bots.ThG_Bots_Bow.ThG_Bots_Max_Bow
            local VWC_Sotre_Bots_Bow_chance = 0
            if VWC_Min_Bots_Bow_chance > VWC_Max_Bots_Bow_chance then
                VWC_Sotre_Bots_Bow_chance = VWC_Min_Bots_Bow_chance
                VWC_Min_Bots_Bow_chance = VWC_Max_Bots_Bow_chance
                VWC_Max_Bots_Bow_chance = VWC_Sotre_Bots_Bow_chance
            end
            return VWC_Min_Bots_Bow_chance, VWC_Max_Bots_Bow_chance

            -- 敌人丢雷
        elseif num == 3 then
            local VWC_Min_Cops_Grenade_chance = VWC_LtyR_Settings_NPC_ThG.Th_Cops.ThG_Cops_Min
            local VWC_Max_Cops_Grenade_chance = VWC_LtyR_Settings_NPC_ThG.Th_Cops.ThG_Cops_Max
            local VWC_Sotre_Cops_Grenade_chance = 0
            if VWC_Min_Cops_Grenade_chance > VWC_Max_Cops_Grenade_chance then
                VWC_Sotre_Cops_Grenade_chance = VWC_Min_Cops_Grenade_chance
                VWC_Min_Cops_Grenade_chance = VWC_Max_Cops_Grenade_chance
                VWC_Max_Cops_Grenade_chance = VWC_Sotre_Cops_Grenade_chance
            end
            return VWC_Min_Cops_Grenade_chance, VWC_Max_Cops_Grenade_chance
        end
    end

    ---描述：这个函数用于指定或根据数字随机选取队友，并返回该队友的位置
    ---@param which:string 指定还是随机，指定打'order'，随机打'list'
    ---@param order:number 在which=='order'时，根据该参数的数字选取单位。1为房主，234为客机。
    ---@param list:number  在which=='list'时，根据该参数选择取多少队友来随机，-1表示全选
    ---@param player:boolean 为真时，在无法获得队友位置时返回玩家位置
    ---@return :Vector3,number 1号位返回被选队友的位置，单位不存在则返回玩家位置或nil；2号位返回房间总客机数
    function VWC_get_a_teammate_position(which, order, list, return_player)
        local peer_itself = {}
        local teammate_number = 1
        local peer_pos = Vector3()
        local can_do = true

        if managers.network and managers.network.session and managers.network:session() then

            for _, bla in ipairs(managers.network:session():peers()) do -- 作为客机遍历会有问题，遍历不全
                if bla then
                    teammate_number = teammate_number + 1
                end
            end

            if which == 'order' then
                peer_itself = managers.network:session():peer(order)
            elseif which == 'list' and teammate_number >= 2 then
                local random_teammate = math.random(2, list)
                if list == -1 then
                    random_teammate = math.random(2, teammate_number)
                end
                peer_itself = managers.network:session():peer(random_teammate)
            end
            if peer_itself and peer_itself.unit and peer_itself:unit() and alive(peer_itself:unit()) then
                peer_pos = peer_itself:unit():position()
            else
                can_do = false
            end
            return can_do and peer_pos or return_player and managers.player and managers.player.player_unit and
                       managers.player:player_unit() and managers.player:player_unit():position() or nil,
                teammate_number - 1

        else
            return
                return_player and managers.player and managers.player.player_unit and managers.player:player_unit() and
                    managers.player:player_unit():position() or nil, teammate_number - 1
        end
    end

    --- 描述：用于在单位活着但是不适合丢雷的时候进行等待，每2秒检测一次是否进入可丢雷状态，如果可以，丢出并重置倒计时；如果不行，继续检测。
    ---@param item:string 丢的啥雷咯
    ---@param unit_self 单位本体
    ---@param player_position:Vector3 玩家位置
    function VWC_btg_Delay_throw(item, unit_self, player_position)
        local min_time, max_time = VWC_btg_Default_time(3)
        local running_time_cop = math.random(min_time, max_time)
        if unit_self and alive(unit_self) then -- 如果单位还存在
            local unit_base = unit_self:base()
            if unit_base then
                if unit_base.VWC_die then -- 这应该不可能触发，但是万一呢？或者以后呢？
                    DelayedCalls:Add("VWC_modticking_cop_die_imp", running_time_cop, VWC_Cop_Throwing_grenades)
                    return
                end
                local dir3 = Vector3() -- init
                local howfar = 900 -- init
                local distance = mvector3.direction(dir3, unit_self:movement():m_com(), player_position)
                if distance < 800 and VWC_btg_Can_throw(unit_self) then

                    unit_base.VWC_wait = false

                    local mov_ext = unit_self:movement()
                    if mov_ext:play_redirect("throw_grenade") then
                        managers.network:session():send_to_peers_synched("play_distance_interact_redirect", unit_self,
                            "throw_grenade")
                    end
                    ProjectileBase.throw_projectile_npc(item, unit_self:movement():m_com(), player_position, unit_self) -- 扔东西
                    DelayedCalls:Add("VWC_modticking_cop_delay_thrown", running_time_cop, VWC_Cop_Throwing_grenades)

                else

                    local wait_time = running_time_cop / 10
                    if wait_time < 2 then
                        wait_time = 2
                    end
                    DelayedCalls:Add("VWC_Delay_throw", wait_time, function()
                        VWC_btg_Delay_throw(item, unit_self, player_position)
                    end)

                end
            end
        else -- 如果单位已经不存在了
            DelayedCalls:Add("VWC_modticking_cop_not_ex", running_time_cop, VWC_Cop_Throwing_grenades)
        end
    end

    --- 描述：用于检测单位是否可以瞄准并看见目标，如果是，返回“真”和'单位对象'；否则返回“假”并根据情况给单位打上等待或者死亡的备注；单位若已不存在，返回"not_exist"
    ---@param unit 单位本身
    function VWC_btg_Can_throw(unit)
        if unit and alive(unit) then
            local attention_data = unit:brain() and unit:brain()._logic_data and unit:brain()._logic_data.attention_obj
            local target_unit = attention_data and attention_data.unit
            local target_verify = attention_data and attention_data.verified
            if target_unit and alive(target_unit) and target_verify then
                return true, target_unit
            else
                local unit_base = unit:base()
                if unit_base then
                    unit_base.VWC_wait = true
                end
                return false
            end
        elseif unit then
            local unit_base = unit:base()
            if unit_base then
                unit_base.VWC_wait = false
                unit_base.VWC_die = true
            end
            return false
        else
            return "not_exist"
        end
    end

    -- **-----------------------------------------------------------------------------------------------------------------------------------------------------**
    -- 暂时用不上，以后可能能用
    --[[function VWC_AF_throwConcussion()
        local randomBotList = {}
        for _, v in pairs(bot) do
            randomBotList[#randomBotList + 1] = v.unit
        end
        local randomBot = randomBotList[math.random(#randomBotList)]
        local randomCop = VWC_AF_randomNearbyCop(800, 5)
        if randomCop ~= nil and randomBot ~= nil then
            -- ProjectileBase.throw_projectile_npc('concussion', randomBot:movement():m_com(), randomBot:movement():m_rot():y(), randomCop)
        end
        local running_time_concussion = math.random(80, 520)
        DelayedCalls:Add("VWC_modticking_concussion", running_time_concussion, VWC_AF_throwConcussion)
    end

    function VWC_AF_split(s, delim)
        if type(delim) ~= "string" or string.len(delim) <= 0 then
            return
        end
        local start = 1
        local t = {}
        while true do
            local pos = string.find(s, delim, start, true) -- plain find
            if not pos then
                break
            end
            table.insert(t, string.sub(s, start, pos - 1))
            start = pos + string.len(delim)
        end
        table.insert(t, string.sub(s, start))
        return t
    end--]]

    --- 描述：获取与who_pos这个位置的距离小于distance的depth个单位，随机返回其中的一个
    ---@param distance:number 
    ---@param depth:number
    ---@param who_pos
    ---@return a random enemy
    function VWC_AF_randomNearbyCop(distance, depth, who_pos)
        if not managers.player:player_unit() then -- ==nil
            return
        end

        local units_data = {}
        local enemies = World:find_units_quick("sphere", who_pos, distance, managers.slot:get_mask("trip_mine_targets"))

        for num, unit in ipairs(enemies) do
            if num > depth then
                break
            end

            units_data[#units_data + 1] = unit
        end

        return units_data[math.random(#units_data)]
    end
    -- **-----------------------------------------------------------------------------------------------------------------------------------------------------**

    function func_Bot_Throwing_grenades()
        local random_grenades_number = math.random(1, 1000)
        local item_grenades = "molotov"
        local has_thrown
        if random_grenades_number < 200 then
            item_grenades = "molotov"
        elseif random_grenades_number < 500 then
            item_grenades = "launcher_frag"
        elseif random_grenades_number < 800 then
            item_grenades = "launcher_incendiary"
        elseif random_grenades_number < 990 then
            item_grenades = "launcher_frag_arbiter"
        elseif random_grenades_number < 1000 then
            item_grenades = "rocket_ray_frag"
        else
            item_grenades = "crossbow_arrow"
        end
        if VariousWeaponForCops and VWC_LtyR_Settings_NPC_ThG.Th_Bots then
            has_thrown = randomUnitsThrowGrenadess(bot, item_grenades, true)
        end
        local min_time, max_time = VWC_btg_Default_time(2)
        local running_time = math.random(min_time, max_time)

        if not has_thrown then
            running_time = running_time / 10
            if running_time > 10 then
                running_time = running_time / 10
            end
            if running_time < 2 then
                running_time = 2
            end
        end
        DelayedCalls:Add("VWC_modticking", running_time, func_Bot_Throwing_grenades)
    end

    function func_Bot_shooting_bow()
        local random_bow_number = math.random(1, 1200)
        local item_bow = "crossbow_arrow_exp"
        local has_thrown
        if random_bow_number < 200 then
            item_bow = "crossbow_arrow_exp"
        elseif random_bow_number < 400 then
            item_bow = "crossbow_poison_arrow"
        elseif random_bow_number < 800 then
            item_bow = "crossbow_arrow"
        elseif random_bow_number < 1000 then
            item_bow = "west_arrow_exp"
        elseif random_bow_number < 1200 then
            item_bow = "bow_poison_arrow"
        else
            item_bow = "crossbow_arrow"
        end
        if VariousWeaponForCops and VWC_LtyR_Settings_NPC_ThG.Th_Bots then
            has_thrown = randomUnitsThrowGrenadess(bot, item_bow, nil)
        end
        local min_time, max_time = VWC_btg_Default_time(1)
        local running_time_bow = math.random(min_time, max_time)

        if not has_thrown then
            running_time_bow = running_time_bow / 10
            if running_time_bow > 10 then
                running_time_bow = running_time_bow / 10
            end
            if running_time_bow < 2 then
                running_time_bow = 2
            end
        end
        DelayedCalls:Add("VWC_modticking_bow", running_time_bow, func_Bot_shooting_bow)
    end

    function VWC_Cop_Throwing_grenades()
        local random_grenades_number = math.random(1, 1000)
        local item_grenades = "molotov"
        local condition = nil
        local unit_check = {}
        local player_pos = {}

        if random_grenades_number < 200 then
            item_grenades = "molotov"
        elseif random_grenades_number < 500 then
            item_grenades = "launcher_frag"
        elseif random_grenades_number < 800 then
            item_grenades = "launcher_incendiary"
        elseif random_grenades_number < 990 then
            item_grenades = "launcher_frag_arbiter"
        elseif random_grenades_number < 1000 then
            item_grenades = "rocket_ray_frag"
        else
            item_grenades = "crossbow_arrow"
        end

        if VariousWeaponForCops and VWC_LtyR_Settings_NPC_ThG.Th_Cops then
            condition, unit_check, player_pos = randomEnemiesThrowGrenadess(item_grenades)
        end

        local min_time, max_time = VWC_btg_Default_time(3)
        local running_time_cop = math.random(min_time, max_time)
        if condition and condition == 'wait' then
            local wait_time = running_time_cop / 10
            if wait_time > 10 then
                wait_time = wait_time / 10
            end
            if wait_time < 2 then
                wait_time = 2
            end
            DelayedCalls:Add("VWC_Delay_throw_start", wait_time, function()
                VWC_btg_Delay_throw(item_grenades, unit_check, player_pos)
            end)
            return
        end

        if condition and condition == "die" then -- 理论上来说这里甚至都不需要检测，单位死了的话根本不会有condition，不过检测一下总比没有好
            running_time_cop = running_time_cop / 10
            if running_time_cop > 10 then
                running_time_cop = running_time_cop / 10
            end
            if running_time_cop < 2 then
                running_time_cop = 2
            end
        end
        DelayedCalls:Add("VWC_modticking_cop", running_time_cop, VWC_Cop_Throwing_grenades)
    end

    --- 描述：这个函数用于选取一个随机AI队友向前方扔雷。成功扔出则返回'true'，未扔出则返回'false'。
    ---@param units_list 单位列表传入此处
    ---@param item 扔啥东西传入此处
    ---@param is_grenade 是不是雷，不是雷就射箭
    function randomUnitsThrowGrenadess(units_list, item, is_grenade)
        -- if #units_list == 0 then 
        -- return
        -- end
        local units_data = {} -- 用于储存单位
        local max = 0
        for _, v in pairs(units_list) do
            max = max + 1
            if max == 10 then
                break
            end
            units_data[#units_data + 1] = v.unit
        end
        local datanumber = math.random(#units_data)
        local chosen_bot = units_data[datanumber]
        if #units_data ~= 0 then
            local can_throw, target_unit = VWC_btg_Can_throw(chosen_bot)
            if can_throw then
                ProjectileBase.throw_projectile_npc(item, chosen_bot:movement():m_com(),
                    chosen_bot:movement():m_rot():y(), chosen_bot)
                if is_grenade then -- 扔雷的话播放音效和动作
                    local mov_ext = chosen_bot:movement()
                    if mov_ext:play_redirect("throw_grenade") then
                        managers.network:session():send_to_peers_synched("play_distance_interact_redirect", chosen_bot,
                            "throw_grenade")
                    end
                    chosen_bot:sound():say("g43", true, true)
                end
                return true
            else
                return false
            end
        end
    end

    --- 描述：这个函数用于选取一个随机执法单位向玩家方向扔雷
    --- BUG：敌方单位要逮捕玩家的时候也会扔雷，敌方单位被控制时也能扔雷
    ---@param item 扔啥东西传入此处
    function randomEnemiesThrowGrenadess(item)

        local player_choose_pos, rnu_mt = VWC_get_a_teammate_position('list', 1, -1, true)
        local rnu_tt = math.random(1, 25 + rnu_mt * 25)
        local throw_to = Vector3()
        if rnu_tt > 25 then
            throw_to = player_choose_pos
        elseif managers.player:player_unit() then
            throw_to = managers.player:player_unit():position()
        else
            return
        end

        local randomCop = VWC_AF_randomNearbyCop(800, 5, throw_to) -- 从800内的5个单位中抓了1个单位出来

        local dir = Vector3() -- init
        local VWC_howfar = 900 -- init

        if randomCop and alive(randomCop) then
            local cop_base = randomCop:base()
            local team_data = randomCop:movement():team() -- 获取单位的阵营
            if team_data.id == "law1" then -- 只让执法单位执行。---或者unit_info:unit():movement():team().id，如果用Hoplib

                VWC_howfar = mvector3.direction(dir, randomCop:movement():m_com(), throw_to) -- 获取该单位到某玩家的距离
                -- local VWC_howfar = math.floor( math.sqrt( math.abs(VWC_po_m_com[1]-VWC_pl_p[1])^2 + math.abs(VWC_po_m_com[2]-VWC_pl_p[2])^2 + math.abs(VWC_po_m_com[3]-VWC_pl_p[3])^2 ))

                -- log("string? "..type(VWC_po_m_com).."and the value? "..VWC_po_m_com)
                if VWC_howfar <= 800 then -- 二次检测以防意外
                    local can_throw = VWC_btg_Can_throw(randomCop)
                    if can_throw and can_throw ~= "not_exist" then
                        local mov_ext = randomCop:movement()
                        if mov_ext:play_redirect("throw_grenade") then
                            managers.network:session():send_to_peers_synched("play_distance_interact_redirect",
                                randomCop, "throw_grenade")
                        end
                        ProjectileBase.throw_projectile_npc(item, randomCop:movement():m_com(), throw_to, randomCop) -- 扔东西
                        -- log("Enemies threw a grenade")
                        return nil
                    elseif can_throw == "not_exist" then -- 死了应该也走不到这一步吧，不过还是以防万一一下
                        return 'die', randomCop, throw_to
                    else
                        if cop_base then
                            if cop_base.VWC_wait then
                                return 'wait', randomCop, throw_to
                            elseif cop_base.VWC_die then -- 死了应该就不存在了吧，不过还是以防万一一下
                                return 'die', randomCop, throw_to
                            else
                                return nil
                            end
                        else
                            return nil
                        end
                    end
                end

            end
        end

    end

    -- AI队友丢雷
    local start_time_min, start_time_max = VWC_btg_Default_time(2)
    local start_time = math.random(start_time_min, start_time_max)
    if VWC_LtyR_Settings_NPC_ThG.Th_Bots.ThG_Bots_Grenade.ThG_Bots_Grenade_Enable then
        DelayedCalls:Add("VWC_firstDelayCall", start_time, func_Bot_Throwing_grenades)
    end

    -- AI队友射箭
    local start_time_bow_min, start_time_bow_max = VWC_btg_Default_time(1)
    local start_time_bow = math.random(start_time_bow_min, start_time_bow_max)
    if VWC_LtyR_Settings_NPC_ThG.Th_Bots.ThG_Bots_Bow.ThG_Bots_Bow_Enable then
        DelayedCalls:Add("VWC_secondDelayCall", start_time_bow, func_Bot_shooting_bow)
    end

    -- 敌人丢雷
    local start_time_concussion_min, start_time_concussion_max = VWC_btg_Default_time(3)
    local start_time_concussion = math.random(start_time_concussion_min, start_time_concussion_max)
    if VWC_LtyR_Settings_NPC_ThG.Th_Cops.ThG_Cops_Enable then
        DelayedCalls:Add("VWC_thridDelayCall", start_time_concussion, VWC_Cop_Throwing_grenades)
    end

end)
